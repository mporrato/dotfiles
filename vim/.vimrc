set nocompatible
set backspace=indent,eol,start
filetype indent plugin on
syntax on
colorscheme elflord
set colorcolumn=79
highlight ColorColumn ctermbg=darkgrey guibg=darkgrey
highlight LineNr ctermbg=darkgrey ctermfg=black guibg=darkgrey guifg=black
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
augroup ftprefs
  autocmd!
  autocmd FileType make set noexpandtab tabstop=8
  autocmd FileType html,xhtml,css,xml,xslt set shiftwidth=2 softtabstop=2
augroup END
set encoding=utf-8
set listchars=tab:▸\ ,eol:¬
set nowrap autoindent
set expandtab tabstop=4 shiftwidth=4
set smartindent autoindent
set hlsearch incsearch showmatch ignorecase smartcase
set hidden wildmenu showcmd 
set ruler visualbell
